''' Note:
Process Flow of this Script: 1. Download Git Project From GitLab Server
                            2. Extract the download tar file
                            3. Translate The C Sharp Project into Common Intermediate Language
                            4. Analyze the project using Fortify
                            5. Use FPRUtility to post-process the analyzer's output
                            6. Delete the download files
'''

import shlex,subprocess,json
import tarfile,glob,os,shutil
import time,traceback
import re,fnmatch
import xml.etree.cElementTree as ET 
from sets import Set
from dateutil.parser import parse
from datetime import datetime
###### Global Variables######
workingDir='D:\\Hard_Coded_Scan'
_7zip='\"C:\\Program Files\\7-Zip\\7z.exe\"'
codeScanner = '\"D:\\Hard_Coded_Scan_Python\\CodeScaner.exe\"'
securityRuleFile= 'D:\\Hard_Coded_Scan_Python\\security_rules.txt'
considerExtFileName='D:\\Hard_Coded_Scan_Python\\considerFile.txt'
currentTime= time.strftime("%Y-%m-%d-%H-%M-%S",time.localtime(time.time()))
logFileName="scan_git_project_for_hard_coded_check_"+currentTime+"_error_log.txt"
projectStatusFileName= "scan_git_project_for_hard_coded_check_"+currentTime+"_status.txt"
scanResultFileName="scan_git_project_for_hard_coded_check_"+currentTime+"_Result.txt"

logFile = None
considerExtFile = None
scanResultFile=None
projectStatusFile=None
userAccount=None

def gitlabAPIQuery(gitlabAPI):
    api_p = subprocess.Popen(gitlabAPI,shell=True,stderr=subprocess.PIPE,stdout=subprocess.PIPE)
    stdout,stderr = api_p.communicate()
    return {'stdout':stdout,'stderr':stderr,'retruncode':api_p.returncode}

def queryLastCommitTime(projectID):
    ccmd = 'curl --header \"PRIVATE-TOKEN: N6bUXRUxX6DWCUqSChhs\" \"http://300cimgit.tsmc.com.tw/api/v3/projects/'+str(projectID)+'/repository/commits/master\"' 
    queryResult=gitlabAPIQuery(ccmd)
    if queryResult['returncode'] == 0:
        commit = json.loads(queryResult['stdout'])
        commitedDate = parse(commit["committed_date"])
        return commitedDate



def checkIfEmptyProject(fileName):
    pattern = '{\"message\":\"404 File Not Found\"}'
    with open(fileName,"r") as f:
        readData = f.read()
        if re.search(pattern,readData) :
            return True
        else:
            return False


def rmUnNecessaryFiles(projectPath):
    
    for fileName in os.listdir(projectPath):
        baseName=os.path.basename(fileName)
        fileName = projectPath+fileName 

        if os.path.isfile(fileName):
            try:
                os.remove(fileName)
            except Exception as e:
                print e
                traceback.print_exc(file=logFile)
        elif os.path.isdir(fileName):
            try:
                shutil.rmtree(fileName)
            except Exception as e :
                print e
                traceback.print_exc(file=logFile)


def executeCodeScanner(projectName,fileName,fileExt):
    try:
        exeCS= codeScanner+" \""+securityRuleFile+"\" \""+fileName+"\""
        exeCS_p = subprocess.Popen(exeCS,shell=True,stderr=subprocess.PIPE,stdout=subprocess.PIPE)
        stdout,stderr = exeCS_p.communicate()
        lines = stdout.split('\n')
        for line in lines:
            if len(line) > 0 :
                scanResultFile.write(userAccount+","+projectName+","+fileName+","+fileExt+","+line+"\n")
        scanResultFile.flush()
        logFile.write("------stderr of codeScanner"+"\n"+stderr)
        logFile.flush()
    except Exception as e:
        print e
        traceback.print_exc(file=logFile)

def createConsiderExtSet():
    ext_set=set()
    with open(considerExtFileName, "r") as considerExtFile:
        for ext in considerExtFile:
            ext=ext.replace("\n","")
            ext_set.add(ext)
    return ext_set


startTime= time.strftime("%Y-%m-%d-%H-%M-%S",time.localtime(time.time()))
print "Start Of Execution: "+ startTime
appRootDir = workingDir
lsUserCmd = 'curl  \"http://300cimgit.tsmc.com.tw/api/v3/users?private_token=N6bUXRUxX6DWCUqSChhs&per_page=100\"'
#llsUserCmd = shlex.split(lsUserCmd)
#return_val = subprocess.check_output(llsUserCmd)
#lsUser_Data = json.loads(return_val)
ucmdResult = gitlabAPIQuery(lsUserCmd)
users = json.loads(ucmdResult['stdout'])
user_count =0
considerFileExt = set()
###### Open log file ######
with  open(logFileName,"w") as logFile,open(projectStatusFileName,"w") as projectStatusFile,open(scanResultFileName,"w") as scanResultFile:
    considerFileExt = createConsiderExtSet()
    for user in users:
        user_count = user_count +1
        if user_count > 0 :
        #if user["username"] == "pychenzi" :
        #if user["username"] == "hctanga":
            print "Start To Process "+user["username"]
            userAccount = user["username"]
            os.chdir(appRootDir) ### Start in application root directory
            pcmd = 'curl  \"http://300cimgit.tsmc.com.tw/api/v3/projects?private_token=N6bUXRUxX6DWCUqSChhs&sudo='+userAccount+'&per_page=500\"'
            ###Create the directory for the user if it doesn't exist###
            userDir=appRootDir+"\\"+userAccount
            if not os.path.exists(userDir):
                os.makedirs(userDir)
            os.chdir(userDir)
            #lCmd = shlex.split(pcmd)
            cwd = os.getcwd()
            ###### Iterate all projects belong to the user 
            pcmdResult = gitlabAPIQuery(pcmd)
            #return_val = subprocess.check_output(lCmd)
            projects = json.loads(pcmdResult['stdout'])
            count=0
            for project in projects:
                os.chdir(userDir)
                print str(project["id"])+"\t"+project["name"]
                logFile.write("----------"+project["name"]+"----------\n")
                logFile.flush()
                count = count+1
                
                #if project["name"] == "S001058_MCOP_ALM_DataLoader" :
                #if project["name"] == "MCOP_APP":
                if count > 0 :
                    command = 'curl -L \"http://300cimgit.tsmc.com.tw/api/v3/projects/'+ str(project["id"])+'/repository/archive.tar.gz?private_token=N6bUXRUxX6DWCUqSChhs\"'
                    lCmd = shlex.split(command)
                    outfile = None
                    try:
                        outfile=open(project["name"]+".tar.gz","w")
                        return_val = subprocess.call(lCmd,stdout=outfile)
                        outfile.close()
                        ###Check file statistics###
                        file_stat = os.stat(outfile.name)
                        print "size of "+outfile.name+":"+str(file_stat.st_size)
                        try:
                            ###### unzip the download file ######
                            ###Check if a valid tar.gz file
                            tar = tarfile.open(outfile.name,"r:gz")
                            tar.close()
                            tarFileName=outfile.name
                            tarFileName2=tarFileName.replace(".gz","")
                            #targetDir = os.path.dirname(tarFileName)
                            extractCmd = _7zip+" e \""+tarFileName+"\" "+" -y && "+_7zip+" x \""+ tarFileName2+"\" -y"
                            
                            #return_val = subprocess.check_output(extractCmd,shell=True)
                            result = gitlabAPIQuery(extractCmd)
                            #extract_p = subprocess.Popen(extractCmd,shell=True,stderr=subprocess.PIPE,stdout=subprocess.PIPE)
                            #stdout,stderr = extract_p.communicate()
                            logFile.write("------stdout of 7-zip Extraction"+"\n"+result['stdout'])
                            logFile.write("------stderr of 7-zip Extraction"+"\n"+result['stderr'])
                            
                            ###change to the shorter name###
                            dirList = glob.glob(cwd+'\\*-master-*'+"\\")
                            fileFindPath = dirList[0]
                            newfileFindPath=re.sub(r'-master-[a-z0-9]+',"",fileFindPath)
                            os.rename(fileFindPath,newfileFindPath)
                            

                            for root,dirnames,filenames in os.walk(newfileFindPath):
                                for filename in filenames:
                                    ab_fileName=os.path.join(root,filename)
                                    if os.path.isfile(ab_fileName):
                                        # only retain the considered files
                                        file_ext= os.path.splitext(ab_fileName)[1].replace(".","")
                                        if  file_ext in considerFileExt:
                                            executeCodeScanner(project["name"],ab_fileName,file_ext)

                            if len(dirList) > 0 :
                                rmUnNecessaryFiles(dirList[0])

                        except tarfile.TarError as e:
                            print e
                            if checkIfEmptyProject(outfile.name):
                                projectStatusFile.write(userAccount+","+project["name"]+",,"+"Empty Project\n")    
                                projectStatusFile.flush()
                        except Exception as e:
                            print e
                            traceback.print_exc(file=logFile)
                            projectStatusFile.write(userAccount+","+project["name"]+",,"+"Faile To Extract tar.gz\n")
                            projectStatusFile.flush()
                    except Exception as e:
                        print e
                        traceback.print_exc(file=logFile)
                        if not outfile.closed:
                                outfile.close()
                            
                    filePath=cwd+"\\"+project["name"]+"*"
                    # Remove the downloaded file and directory
                    for file in glob.glob(filePath):
                        if os.path.isfile(file):
                            try:
                                os.remove(file)
                            except Exception as e:
                                print e
                                traceback.print_exc(file=logFile)
                        #elif os.path.isdir(file):
                        #    shutil.rmtree(file)
endTime= time.strftime("%Y-%m-%d-%H-%M-%S",time.localtime(time.time()))
print "End Of Execution: "+ endTime
